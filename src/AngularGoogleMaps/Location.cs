﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps
{
    public class Model
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public int Zoom { get; set; }

        public Model()
        {

        }

        public Model(Model other)
        {
            Latitude = other.Latitude;
            Longitude = other.Longitude;
            Zoom = other.Zoom;
        }

        private static bool String2Model(Model location, string value)
        {
            if (value == null)
            {
                return false;
            }

            var args = ((string)value).Split(',');
            if (args.Length < 3)
            {
                return false;
            }

            decimal lat, lng;
            int zoom;
            if (!decimal.TryParse(args[0], NumberStyles.Any, CultureInfo.InvariantCulture, out lat))
            {
                return false;
            }
            if (!decimal.TryParse(args[1], NumberStyles.Any, CultureInfo.InvariantCulture, out lng))
            {
                return false;
            }
            if (!int.TryParse(args[2], out zoom))
            {
                return false;
            }

            location.Latitude = lat;
            location.Longitude = lng;
            location.Zoom = zoom;
            return true;
        }

        private static bool Model2String(Model location, ref string value)
        {
            if (location.Zoom < 1)
            {
                value = "";
                return false;
            }

            StringBuilder output = new StringBuilder();
            output.Append(Math.Round(location.Latitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(',');
            output.Append(Math.Round(location.Longitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(',');
            output.Append(location.Zoom);
            value = output.ToString();
            return true;
        }

        public static implicit operator Model(string value)
        {
            return new Model(value);
        }

        public Model(string value)
        {
            String2Model(this, value);
        }

        public static implicit operator string(Model location)
        {
            string value = "";
            Model2String(location, ref value);
            return value;
        
        }

        public override string ToString()
        {
            string value = "";
            Model2String(this, ref value);
            return value;
        }
    }
}
