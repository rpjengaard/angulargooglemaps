﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using System.Threading;
using System.Collections.Generic;

namespace AngularGoogleMaps.Test
{
    [TestClass]
    public class UnitTest1
    {
        //  See if we are correctly setting culture
        [TestMethod]
        public void Culture()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("");
            double d = -1.01;
            Assert.AreEqual(d.ToString(), "-1.01");

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Assert.AreEqual(d.ToString(), "-1.01");

            Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");
            Assert.AreEqual(d.ToString(), "-1,01");

            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            Assert.AreEqual(d.ToString(), "-1,01");

        }

        [TestMethod]
        public void Convert()
        {
            var cultures = new string[]
            {
                "",
                "en-US",
                "en-GB",
                "ar-EG",
                "ar-SA",
                "zh-CN",
                "hr-HR",
                "cs-CZ",
                "da-DK",
                "nl-NL",
                "fa-IR",
                "fi-FI",
                "fr-FR",
                "de-DE",
                "el-GR",
                "gu-IN",
                "hi-IN",
                "hu-HU",
                "it-IT",
                "ja-JP",
                "ko-KR",
                "pl-PL",
                "es-ES",
                "tr-TR",
                "vi-VN"
            };

            var values = new string[]
            {
                "1,2,3",
                "-1,-2,3",
                "1,-2,3",
                "-1,2,3",

                "1.2,3.4,4",
                "-1.2,3.4,4",
                "1.2,-3.4,4",
                "-1.2,-3.4,4",

                "100.2,300.4,5",
                "-100.2,300.4,5",
                "100.2,-300.4,5",
                "-100.2,-300.4,5",

                "0.1,0.2,6",
                "-0.1,0.2,6",
                "0.1,-0.2,6",
                "-0.1,-0.2,6",

                "0.02,0.03,7",
                "-0.02,0.03,7",
                "0.02,-0.03,7",
                "-0.02,-0.03,7",

                "0.02,0.03,7",
                "-0.02,0.03,7",
                "0.02,-0.03,7",
                "-0.02,-0.03,7",

                "123.456789,987.654321,8",
                "-123.456789,987.654321,8",
                "123.456789,-987.654321,8",
                "-123.456789,-987.654321,8",

                "123.000009,987.000001,9",
                "-123.000009,987.000001,9",
                "123.000009,-987.000001,9",
                "-123.000009,-987.000001,9",

                "100.000009,900.000001,10",
                "-100.000009,900.000001,10",
                "100.000009,-900.000001,10",
                "-100.000009,-900.000001,10"
            };

            foreach (var culture in cultures)
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);

                System.Console.WriteLine(culture);
                var d = -1.01;
                System.Console.WriteLine(1.10);
                
                foreach (var value in values)
                {
                    var model = new AngularGoogleMaps.Model(value);
                    var compareValue = model.ToString();
                    Assert.AreEqual(value, compareValue);
                }
            }
        }
    }
}
